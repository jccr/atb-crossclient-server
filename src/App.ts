import * as express from 'express';
import * as logger from 'morgan';
import "reflect-metadata";
import {useContainer, useExpressServer} from "routing-controllers";
import {Container} from "typedi";

const TEST_MODE = !!process.env['LOADED_MOCHA_OPTS'];

// Creates and configures an ExpressJS web server.
class App {

    // ref to Express instance
    public express: express.Application;

    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    // Configure Express middleware.
    private middleware(): void {
        this.express.use(logger('dev'));
    }

    // Configure API endpoints.
    private routes(): void {
        // setup routing-controllers to use typedi container. You can use any container here
        useContainer(Container);
        // use express server
        useExpressServer(this.express, {
            controllers: [__dirname + `/controllers/*.${TEST_MODE ? 'ts' : 'js'}`]
        });
    }
}

export default new App().express;