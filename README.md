# ATB CrossClient Server

1. Install all dependencies:

    `npm install`
    
2. Compile the project:

    `npm run build`

3. Run the project:

    `npm start`
    
4. Open in browser:
 
    `http://localhost:3000/posts`
