import * as mocha from 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

import app from '../src/App';
import {Post} from "../src/model/Post";

chai.use(chaiHttp);
const expect = chai.expect;

describe('GET /posts', () => {

    it('responds with JSON array', () => {
        return chai.request(app).get('/posts')
            .then(res => {
                expect(res.status).to.equal(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('array');
                expect(res.body).to.have.length(4);
            });
    });

    it('should include a post about Elon Musk', () => {
        return chai.request(app).get('/posts')
            .then(res => {
                let elonMuskPost = res.body.find((post: Post) => post.title === 'Elon Musk');
                expect(elonMuskPost).to.exist;
                expect(elonMuskPost).to.have.all.keys([
                    'id',
                    'title',
                    'text',
                    'createDate',
                    'categories',
                ]);
            });
    });

    it('should include a post with categories', () => {
        return chai.request(app).get('/posts')
            .then(res => {
                let applePost = res.body.find((post: Post) => post.title === 'Apple');
                expect(applePost).to.exist;
                expect(applePost).to.have.any.keys('categories');
                expect(applePost.categories).to.be.an('array');
                expect(applePost.categories).to.not.be.empty;
                let category = applePost.categories[0];
                expect(category).to.exist;
                expect(category).to.have.all.keys([
                    'id',
                    'name'
                ]);
            });
    });

    describe('GET /posts/:id', () => {

        it('responds with single JSON object', () => {
            return chai.request(app).get('/posts/3')
                .then(res => {
                    expect(res.status).to.equal(200);
                    expect(res).to.be.json;
                    expect(res.body).to.be.an('object');
                });
        });

        it('should return a post about Justin Trudeau', () => {
            return chai.request(app).get('/posts/3')
                .then(res => {
                    expect(res.body.title).to.equal('Justin Trudeau');
                });
        });

    });

});